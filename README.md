<h1>API Visualisations Modul </h1>
Dieses Modul dient der Visualisation der Ergebnisse. Ebenso errechnet es viele der Abbildungen in den Tests.
Um dies zu bewerkstelligen sind einige Vorkehrungen von nöten.

<h4>Notwendige Informationen vor dem Start:</h4>

1. Ausgeführte Analyse
1. Verbindung zur Datenbank der Analyse

### Generation von Daten
Dieses Modul dient hauptsächlich zur Auswertung der Daten für die Zufalls Analyse. Für die Generierung von Karten stellt der
Service Rest Endpoints zur Verfügung. Die Ausgabe der Tabellen und anderer Daten geschieht in den Tests.


<h3>Module Einstellungen</h3>
Dadurch, dass dieses Modul auf dem Analysator aufbaut, um mehrere Datenklassen einfacher zu laden sind ebenso dieselben Services erreichbar.
Diese sind aber, um ein schnelleres Starten zu gewährenleisten, in der ``application.yml`` deaktiviert worden. 


Config | Default | Beschreibung
-------- | -------- | --------
api.url   | -   | Endpoint URL zum Abfragen von Geolokalisationsanfragen
init.loadFromJson  | false   | Städte aus JSON laden anstelle aus Datenbank
cityquery.enable  | false   | Erstellen von Stichproben der Analyse (Voraussetzung Städte müssen bereits geladen worden sein) ``RequestCalculator`` muss einmal ausgeführt worden sein
requestCalculator.enable | true   | Erstellt Städte in der Datenbank und legt erlaubte Stichproben pro Stadt fest
cityAnalyse.enable | false   | Analysiert die Ergebnisse um statistische Daten zu sammeln. (Daten zusätzlich)
adressEvaluation.enable  | true   | Erstellt statistische Daten pro Stadt, welche zur Auswertung in API-Visualisation genutzt werden
groupingCategory.enable  | true | gruppiert die Städte nach einigen Kategorien um bessere Auswertungen erstellen zu können.
categorizer.enable  | true   | Prüft korrekt angenommene Stichproben und teilt diese in Gruppen ein
distanceEvaluation.enable  | true | Erstellt Distanzen für Städte
extra-information.enable  | true | Erhöht die Stichprobengröße auf eine durch 3 teilbare Zahl
randomanalyse.enable | true    | Falls ``Categorizer`` enabled ist werden alle Anfragen Kategorisiert und bei nicht korrekten Anfragen in den Daten werden die fehlerhaften Anfragen neu kategorisiert

