package com.example.apivisualisation.data;

import com.example.apivisualisation.service.CityEvaluationResultLoader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
class DistanceEvaluatorTest {
    @Autowired
    DistanceEvaluator distanceEvaluator;
    @Autowired
    CityEvaluationResultLoader resultLoader;

    @Test
    void getDistanceResult() {
        // uses not the plz saved for frankfurt
        var distanceFrankfurt = distanceEvaluator.getDistanceResult("60306.0", "Here-APIGoogleAPI", true);
        assertNull(distanceFrankfurt);
        //var res = distanceEvaluator.getDistanceResult("ALL", "GoogleAPINominatim-API", false);
        var nominatim = distanceEvaluator.getDistanceResult("ALL", "GoogleAPINominatim-API", false);
        //var nominatim = distanceEvaluator.getDistanceResult("ALL", "GoogleAPINominatim-API", true);
        //var here = distanceEvaluator.getDistanceResult("ALL", "GoogleAPIHere-API", true);
        var here = distanceEvaluator.getDistanceResult("ALL", "GoogleAPIHere-API", false);
        System.out.println(nominatim);
        System.out.println(here);
        assertNotNull(here);
        for (var correct : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                System.out.println(correct + service);
                for (var state : resultLoader.getCitiesByState().keySet()) {
                    if (!correct.equals(service)) {
                        var result = distanceEvaluator.getDistanceResult(state, correct + service, false);
                        var penalty = distanceEvaluator.getDistanceResult(state, correct + service, true);
                        System.out.printf("%s & %.2fm(%.2fm) & %.2fm & %.2fm & %.2fm \\\\ \n", state,
                                penalty.getAverage(),
                                result.getAverage(),
                                penalty.getMedian(),
                                penalty.getPercentile95(),
                                penalty.getPercentile99());
                        System.out.println("\\hline");
                    }
                }
            }
        }
        for (var correct : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            System.out.println("Correct :" + correct);
            for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                if (!correct.equals(service)) {

                    var result = distanceEvaluator.getDistanceResult("ALL", correct + service, false);
                    var penalty = distanceEvaluator.getDistanceResult("ALL", correct + service, true);
                    System.out.printf("%s & %.2fm(%.2fm) & %.2fm & %.2fm & %.2fm \\\\ \n", service,
                            penalty.getAverage(),
                            result.getAverage(),
                            penalty.getMedian(),
                            penalty.getPercentile95(),
                            penalty.getPercentile99());
                    System.out.println("\\hline");
                }
            }
        }

        System.out.println("ALL Distances over hundert meter");

        var distances = distanceEvaluator.getDistanceCollector("ALL").getOverHundertMeter();
        System.out.println(distances);

        System.out.println("Distance per size of city");


            for (var correct : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                    System.out.println(correct + service);
                    for (var citySize : List.of("große Großstadt", "kleinere Großstadt", "große Mittelstadt",
                            "kleine Mittelstadt", "große Kleinstadt", "kleine Kleinstadt", "kleine Kleinstadt", "Landstadt", "Dorf")) {
                        if (!correct.equals(service)) {
                            var result = distanceEvaluator.getDistanceResult(citySize, correct + service, false);
                            var penalty = distanceEvaluator.getDistanceResult(citySize, correct + service, true);
                            System.out.printf("%s & %.2fm(%.2fm) & %.2fm & %.2fm & %.2fm \\\\ \n", citySize,
                                    penalty.getAverage(),
                                    result.getAverage(),
                                    penalty.getMedian(),
                                    penalty.getPercentile95(),
                                    penalty.getPercentile99());
                            System.out.println("\\hline");
                        }
                    }

            }

        }

    }
}

