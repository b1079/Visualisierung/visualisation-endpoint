package com.example.apivisualisation.endpoint;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class RequestEndpointTest {
    @Autowired
    RequestEndpoint requestEndpoint;
    @Autowired
    DistanceEndpoint distanceEndpoint;

    @Test
    void plzforCity() {
        // 2 response: Aachen 52074 1.
        var res = requestEndpoint.plzforCity("Aachen", "52074");
        System.out.println(res);

        assertEquals("52058.0", res);
        var frankfurt = requestEndpoint.plzforCity("Frankfurt am Main", "60306.0");
        assertNotNull(frankfurt);
        assertEquals("60311.0", frankfurt);
    }
}
