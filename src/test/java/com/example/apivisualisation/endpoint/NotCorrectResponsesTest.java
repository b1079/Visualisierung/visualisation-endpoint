package com.example.apivisualisation.endpoint;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class NotCorrectResponsesTest {
    @Autowired
    NotCorrectResponses notCorrectResponses;

    @Test
    void notCorrectResponsesPerPlz() {
        double res = notCorrectResponses.notCorrectResponsesPerPlz("55234", "Nominatim-API");
        assertTrue(res > 0);
        System.out.println(res);

        double test2 = notCorrectResponses.notCorrectResponsesPerPlz("52058", "Nominatim-API");
        System.out.println(test2);
    }



    @Test
        // generate NO_HOUSE_NUMBER categories per state
    void averagesForStates() {
        System.out.println("Average for states");
        for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            var res = notCorrectResponses.averagesForStates(service);
            System.out.println(service);
            for (var state : res.keySet())
                System.out.printf(" %s: %.2f %n", state, res.get(state) * 100);
        }
    }

    @Test
    void averagesForPopulation() {
        for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            System.out.println("Average for population");
            var res = notCorrectResponses.noStreetsByCitySize(service, "city_type");
            System.out.println(service);
            for (var state : res.keySet())
                System.out.printf(" %s: %.2f %n", state, res.get(state) * 100);
        }
    }
}
