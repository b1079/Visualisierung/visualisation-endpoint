package com.example.apivisualisation.endpoint;

import com.example.analyser.randomAnalyse.data.address.AddressCategory;
import com.example.apivisualisation.service.CityEvaluationResultLoader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class AdressEndpointTest {
    @Autowired
    AdressEndpoint adressEndpoint;
    @Autowired
    CityEvaluationResultLoader cityEvaluationResultLoader;

    @Test
    void getDistanceForPlz() {

        System.out.println("ALL Results");
        for (var correctAccepted : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            System.out.printf("Correct Accepted: %s\n", correctAccepted);
            var otherServices = new ArrayList<>(List.of("Nominatim-API", "Here-API", "GoogleAPI"));
            otherServices.remove(correctAccepted);
            var firstTestService = adressEndpoint.getAdressResults("ALL", otherServices.get(0), correctAccepted, false);
            var secondTestService = adressEndpoint.getAdressResults("ALL", otherServices.get(1), correctAccepted, false);
            System.out.println(otherServices);
            for (var category : firstTestService.getCategoryCount().keySet()) {
                if (firstTestService.getDowngradeCountCorrect().containsKey(category))
                    System.out.printf("%s & %d (%d) & %d (%d) \\\\ \n", category, firstTestService.getCategoryCount().get(category),
                            firstTestService.getDowngradeCountCorrect().get(category),
                            secondTestService.getCategoryCount().get(category),
                            secondTestService.getDowngradeCountCorrect().get(category));
                else
                    System.out.printf("%s & %d & %d \\\\ \n", category, firstTestService.getCategoryCount().get(category),
                            secondTestService.getCategoryCount().get(category));

            }

        }
        System.out.println("ALL Betrachtung");
        for (var correctAccepted : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            System.out.printf("Correct Accepted: %s\n", correctAccepted);
            var otherServices = new ArrayList<>(List.of("Nominatim-API", "Here-API", "GoogleAPI"));
            System.out.println("Testing Service " + otherServices );
            for (var testingService : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                if(correctAccepted.equals(testingService))
                    continue;
                var result = adressEndpoint
                        .getAdressResults("ALL", testingService, correctAccepted, false)
                        .getDistancesPerCategory().get(AddressCategory.SAME_STREET.toString());
                var penalty = adressEndpoint
                        .getAdressResults("ALL", testingService, correctAccepted, true)
                        .getDistancesPerCategory().get(AddressCategory.SAME_STREET.toString());

                System.out.printf("%s & %.2fm(%.2fm) & %.2fm & %.2fm & %.2fm \\\\ \n", testingService,
                        penalty.getAverage(),
                        result.getAverage(),
                        penalty.getMedian(),
                        penalty.getPercentile95(),
                        penalty.getPercentile99());
                System.out.println("\\hline");
            }
        }

        System.out.println("Bundesländer Betrachtung");
        // distances only for same address results

        for (var correctAccepted : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            System.out.printf("Correct Accepted: %s\n", correctAccepted);
            for (var testingService : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
                System.out.println("Testing Service " + testingService);
                for (var bundesland : cityEvaluationResultLoader.getCitiesByState().keySet()) {
                    if (!testingService.equals(correctAccepted)) {
                        var result = adressEndpoint
                                .getAdressResults(bundesland, testingService, correctAccepted, false)
                                .getDistancesPerCategory().get(AddressCategory.SAME_ADDRESS.toString());
                        var penalty = adressEndpoint
                                .getAdressResults(bundesland, testingService, correctAccepted, true)
                                .getDistancesPerCategory().get(AddressCategory.SAME_ADDRESS.toString());

                        System.out.printf("%s & %.2fm(%.2fm) & %.2fm & %.2fm & %.2fm \\\\ \n", bundesland,
                                penalty.getAverage(),
                                result.getAverage(),
                                penalty.getMedian(),
                                penalty.getPercentile95(),
                                penalty.getPercentile99());
                        System.out.println("\\hline");

                    }
                }
            }
        }

    }
}
