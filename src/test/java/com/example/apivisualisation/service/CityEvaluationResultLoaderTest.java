package com.example.apivisualisation.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class CityEvaluationResultLoaderTest {
    @Autowired
    CityEvaluationResultLoader resultLoader;

    @Test
    /**
     * gives out the categories occurance of each service
     */
    void getAllCities() {
        assertTrue(resultLoader.getCategoryCount().keySet().size() > 0);
        for (var key : resultLoader.getCategoryCount().keySet()) {
            var service = key.split("[.]")[0];
            var error = key.split("[.]")[1];
            System.out.printf("%s %s  %d \n", service, error, resultLoader.getCategoryCount().get(key));
        }
    }

    @Test
    /**
     * get address results
     */
    void getAddressCategoriesByService() {
        Map<String, Map<String, Map<String, Integer>>> correctAcceptedService = new HashMap<>();
        for (var service : resultLoader.getGlobalAdressCategory().keySet()) {
            System.out.println("Test Service: " + service);
            for (var key : resultLoader.getGlobalAdressCategory().get(service).keySet()) {
                var result = resultLoader.getGlobalAdressCategory().get(service).get(key);
                var category = key.split("[-]", 2)[0];
                var correctAccepted = key.split("[-]", 2)[1];
                if (!correctAcceptedService.containsKey(correctAccepted)) {
                    correctAcceptedService.put(correctAccepted, new HashMap<>());
                }
                var serviceMap = correctAcceptedService.get(correctAccepted);
                if (!serviceMap.containsKey(service)) {
                    serviceMap.put(service, new HashMap<>());
                }
                var categoryMap = serviceMap.get(service);
                if (!categoryMap.containsKey(category)) {
                    categoryMap.put(category, 0);
                }
                categoryMap.put(category, categoryMap.get(category) + result);
            }
        }
        correctAcceptedService.forEach((key, map) -> {
                    System.out.println("Correct Accepted" + key);
                    System.out.println(map);
                }
        );
    }
}
