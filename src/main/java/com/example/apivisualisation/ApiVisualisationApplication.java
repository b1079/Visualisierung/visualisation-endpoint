package com.example.apivisualisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {
        "com.example.analyser",
        "com.example.apivisualisation"
})

public class ApiVisualisationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiVisualisationApplication.class, args);
    }

}
