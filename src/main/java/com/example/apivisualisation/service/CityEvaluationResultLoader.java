package com.example.apivisualisation.service;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.repos.CityEvaluationRepository;
import com.example.analyser.randomAnalyse.repos.CityTestingDataRepository;
import com.example.analyser.utility.SimilarityService;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Service
public class CityEvaluationResultLoader {
    CityTestingDataRepository cityTestingDataRepository;
    CityEvaluationRepository cityEvaluationRepository;
    SimilarityService similarityService;
    Map<String, List<CityEvaluation>> allCitiesByPlz = new HashMap<>();
    Map<String, List<CityEvaluation>> allCitiesByName = new HashMap<>();
    Map<String, List<CityEvaluation>> citiesByState = new HashMap<>();
    Map<String, Integer> categoryCount = new HashMap<>();
    Map<String, Map<String, Integer>> globalAdressCategory = new HashMap<>();
    Map<String, List<CityEvaluation>> citiesByAlias = new HashMap<>();
    List<CityEvaluation> allCities;

    CityEvaluationResultLoader(CityTestingDataRepository cityTestingDataRepository, CityEvaluationRepository cityEvaluationRepository, SimilarityService similarityService) {
        this.cityEvaluationRepository = cityEvaluationRepository;
        this.cityTestingDataRepository = cityTestingDataRepository;
        this.similarityService = similarityService;
        allCities = cityEvaluationRepository.findAll();
        for (var cityEval : allCities) {
            initMaps(cityEval);
            countAddressResults(cityEval);
        }
    }

    private void initMaps(CityEvaluation cityEvaluation) {
        String cityName = cityEvaluation.getCity().getGemeindename();
        String state = cityEvaluation.getCityEvaluationStatisticData().getCityCategories().get("state");
        String plz = cityEvaluation.getCity().getPlz();
        String alias = cityEvaluation.getAlias();
        if (!allCitiesByPlz.containsKey(plz)) {
            allCitiesByPlz.put(plz, new ArrayList<>());
        }
        if (!allCitiesByName.containsKey(cityName)) {
            allCitiesByName.put(cityName, new ArrayList<>());
        }
        if (!citiesByState.containsKey(state)) {
            citiesByState.put(state, new ArrayList<>());
        }
        if (!citiesByAlias.containsKey(alias))
            citiesByAlias.put(alias, new ArrayList<>());
        countCategories(cityEvaluation);
        allCitiesByPlz.get(plz).add(cityEvaluation);
        allCitiesByName.get(cityName).add(cityEvaluation);
        citiesByState.get(state).add(cityEvaluation);
        citiesByAlias.get(alias).add(cityEvaluation);
    }

    private void countCategories(CityEvaluation cityEvaluation) {
        var categoriesPerService = cityEvaluation.getCityEvaluationStatisticData().getCategoryPerService();
        for (var category : categoriesPerService.keySet()) {
            for (var service : categoriesPerService.get(category).keySet()) {
                if (!categoryCount.containsKey(service + "." + category))
                    categoryCount.put(service + "." + category, 0);
                categoryCount.put(service + "." + category, categoryCount.get(service + "." + category) + categoriesPerService.get(category).get(service));
            }
        }
    }

    private void countAddressResults(CityEvaluation cityEvaluation) {
        var addressEval = cityEvaluation.getAddressStatisticData();
        for (var service : List.of("Nominatim-API", "Here-API", "GoogleAPI")) {
            var result = addressEval.getAddressResultForService().get(service);
            if (!globalAdressCategory.containsKey(service))
                globalAdressCategory.put(service, new HashMap<>());
            var serviceMap = globalAdressCategory.get(service);

            for (var correctAccepted : result.getAddressDistanceByCorrectAccepted().keySet()) {
                var results = result.getAddressDistanceByCorrectAccepted().get(correctAccepted);
                for (var res : results) {
                    var key = res.getAddressCategory() + "-" + correctAccepted;
                    if (!serviceMap.containsKey(key)) {
                        serviceMap.put(key, 0);
                    }
                    serviceMap.put(key, serviceMap.get(key) + 1);
                }
            }
        }
    }


}
