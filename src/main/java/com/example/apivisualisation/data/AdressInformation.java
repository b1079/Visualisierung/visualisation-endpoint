package com.example.apivisualisation.data;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class AdressInformation {

    Map<String, Integer> categoryCount = new HashMap<>();
    Map<String, Integer> downgradeCountCorrect = new HashMap<>();
    Map<String, Integer> downgradeNoHouseNumber = new HashMap<>();
    Map<String, Distances> distancesPerCategory = new HashMap<>();

}
