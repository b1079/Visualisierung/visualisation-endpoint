package com.example.apivisualisation.data;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityTestingData;
import lombok.AllArgsConstructor;
import lombok.Data;
import model.geometry.GeoJSONPoint;

import java.util.List;
@Data
@AllArgsConstructor
public class InformationForCity {
    String cityName;
    GeoJSONPoint locationOfCity;
    List<CityTestingData> cityTestingData;

}
