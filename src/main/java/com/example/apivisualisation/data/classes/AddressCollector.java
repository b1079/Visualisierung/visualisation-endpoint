package com.example.apivisualisation.data.classes;

import com.example.analyser.randomAnalyse.data.address.AddressCategory;
import com.example.analyser.randomAnalyse.data.address.AddressDistance;
import com.example.analyser.randomAnalyse.data.address.AddressResult;
import com.example.analyser.randomAnalyse.data.cityevaluation.Category;
import com.example.apivisualisation.data.AdressInformation;
import com.example.apivisualisation.data.Distances;

import java.util.*;
import java.util.stream.Collectors;

public class AddressCollector {
    Map<String, List<AddressDistance>> adressDataMap = new HashMap<>();

    public void add(String key, AddressResult result, String serviceToTest) {
        var correctAcceptedMap = result.getAddressDistanceByCorrectAccepted();
        if (!adressDataMap.containsKey(key))
            adressDataMap.put(key, new ArrayList<>());
        var res = adressDataMap.get(key);
        res.addAll(correctAcceptedMap.get(serviceToTest));
    }

    public AdressInformation getInformation(String key, boolean distancePenalty) {
        AdressInformation adressInformation = new AdressInformation();
        Map<String, Integer> categoryCount = new HashMap<>();
        Map<String, Distances> distancesPerCategory = new HashMap<>();
        Map<String, Integer> downgradeCountCorrect = new HashMap<>();
        Map<String, Integer> downgradeCountNoHousenumber = new HashMap<>();
        adressInformation.setCategoryCount(categoryCount);
        adressInformation.setDistancesPerCategory(distancesPerCategory);
        adressInformation.setDowngradeCountCorrect(downgradeCountCorrect);
        adressInformation.setDowngradeNoHouseNumber(downgradeCountNoHousenumber);
        for (var addressDistance : adressDataMap.get(key)) {
            var addressCategory = addressDistance.getAddressCategory();
            if (!categoryCount.containsKey(addressCategory)) {
                categoryCount.put(addressCategory, 0);
                downgradeCountCorrect.put(addressCategory, 0);
                downgradeCountNoHousenumber.put(addressCategory,0);
            }
            categoryCount.put(addressCategory, categoryCount.get(addressCategory) + 1);
            // init diff between correct and not correct
            if (addressDistance.getOriginalRequestCategory().equals(Category.CORRECT.toString()) &&
                    !addressDistance.getAddressCategory().equals(AddressCategory.SAME_ADDRESS.toString())) {
                downgradeCountCorrect.put(addressCategory, downgradeCountCorrect.get(addressCategory) + 1);
            }
            if (addressDistance.getOriginalRequestCategory().equals(Category.NO_HOUSE_NUMBER.toString()) &&
                    !addressDistance.getAddressCategory().equals(AddressCategory.SAME_STREET.toString())) {
                downgradeCountNoHousenumber.put(addressCategory, downgradeCountNoHousenumber.get(addressCategory) + 1);
            }
        }
        for (var category : categoryCount.keySet()) {
            var addresses = adressDataMap.get(key);
            var categoryList = addresses.stream().
                    filter(o -> o.getAddressCategory()
                            .equals(category)).map(AddressDistance::getDistance).collect(Collectors.toList());
            distancesPerCategory.put(category, calculateResult(categoryList, key, distancePenalty));
        }
        return adressInformation;
    }

    private Distances calculateResult(List<Double> results, String key, boolean penalty) {
        Distances distances = new Distances();
        if (penalty) {
            for (int i = 0; i < results.size(); i++) {
                if (results.get(i) > 300d)
                    results.set(i, 300d);
            }
        }
        Collections.sort(results);
        distances.setKey(key);
        if (results.size() == 0) {
            // in case that no valid request could not be found for service in the area (min per city 2)
            // no_house_number are excluded. This results in nominatim and other services sometimes having no datapoint
            // return value to 301 to differentiate between error and no-data
            distances.setMedian(301);
            distances.setPercentile90(301);
            distances.setPercentile95(301);
            distances.setPercentile99(301);
        } else {
            distances.setMedian(results.get((int) Math.ceil((double) results.size() / 2) - 1));
            distances.setPercentile90(results.get(getPercentileIndex(90, results.size())));
            distances.setPercentile95(results.get(getPercentileIndex(95, results.size())));
            distances.setPercentile99(results.get(getPercentileIndex(99, results.size())));
        }

        distances.setAverage(results.stream().mapToDouble(value -> value).average().orElse(301d));
        return distances;
    }

    private int getPercentileIndex(int percentile, int size) {
        double res = (double) size / 100 * percentile;
        // example: size 100 , 95% percentile => 1 * 95 => 95 - 1 (starts at 0)=> 94
        // example size: 25, 99% => 0.25 * 99 => 25 - 1 (index reduction) => 24
        return (int) Math.ceil(res) - 1;
    }


}
