package com.example.apivisualisation.data.classes;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.data.distance.DistanceResult;
import com.example.apivisualisation.data.Distances;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * class to manage and collect the distances for each service.
 * Key represents the combination of correct accepted service and service which delivered the result
 * for example: "NominatimGoogle" => is the combination of the correct datapoint from nominatim and the distance result
 * of google
 * special cases: In case a service did not respond with a answer to the original requested data the distance is -1.0
 */
@Slf4j
@Getter
public class DistanceCollector {
    Map<String, ArrayList<Double>> distanceMap = new HashMap<>();
    Map<String, Integer> noResponseCounts = new HashMap<>();
    HashMap<String, Integer> overHundertMeter = new HashMap<>();
    String cityName;

    public void add(String key, DistanceResult distanceEvaluation, CityEvaluation cityEval, int requestNumber, String service) {
        this.cityName = cityEval.getCity().getGemeindename();
        if (!distanceMap.containsKey(key)) {
            distanceMap.put(key, new ArrayList<>());
            noResponseCounts.put(key, 0);
        }

        var sortedList = distanceMap.get(key);
        var noResponse = noResponseCounts.get(key);

        if (!distanceEvaluation.isHasFullAddress()) {
            return;
        }
        var serviceIndex = distanceEvaluation.getOtherApis().indexOf(service);
        var distance = distanceEvaluation.getDistance().get(serviceIndex);
        if (distance == null) {
            // NO Original Request information == Location of correct Accepted is empty
            // can´t add penalty
            log.error(key);
        } else if (distance < 0) {
            // NO response of service
            noResponse = noResponse + 1;
            noResponseCounts.put(key, noResponse);
        } else {
            // response
            var dist = distanceEvaluation.getDistance().get(serviceIndex);
            if (dist > 100d) {
                if (!overHundertMeter.containsKey(key))
                    overHundertMeter.put(key, 0);
                overHundertMeter.put(key, overHundertMeter.get(key) + 1);
            }
            sortedList.add(dist);
        }

    }



    private Distances calculateResult(String key, boolean penalty) {
        Distances distances = new Distances();
        ArrayList<Double> results = distanceMap.get(key);

        if (penalty) {
            for (int i = 0; i < results.size(); i++) {
                if (results.get(i) > 300d)
                    results.set(i, 300d);
            }
        }
        addPenalty(key, results);
        Collections.sort(results);
        distances.setKey(key);
        if (results.size() == 0) {
            // in case that no valid request could not be found for service in the area (min per city 2)
            // no_house_number are excluded. This results in nominatim and other services sometimes having no datapoint
            // return value to 301 to differentiate between error and no-data
            distances.setMedian(301);
            distances.setPercentile90(301);
            distances.setPercentile95(301);
            distances.setPercentile99(301);
        } else {
            distances.setMedian(results.get((int) Math.ceil((double) results.size() / 2) - 1));
            distances.setPercentile90(results.get(getPercentileIndex(90, results.size())));
            distances.setPercentile95(results.get(getPercentileIndex(95, results.size())));
            distances.setPercentile99(results.get(getPercentileIndex(99, results.size())));
        }

        distances.setAverage(results.stream().mapToDouble(value -> value).average().orElse(301d));
        return distances;
    }

    private int getPercentileIndex(int percentile, int size) {
        double res = (double) size / 100 * percentile;
        // example: size 100 , 95% percentile => 1 * 95 => 95 - 1 (starts at 0)=> 94
        // example size: 25, 99% => 0.25 * 99 => 25 - 1 (index reduction) => 24
        return (int) Math.ceil(res) - 1;
    }

    private ArrayList<Double> addPenalty(String key, ArrayList<Double> list) {
        if (noResponseCounts.containsKey(key)) {
            int noResponses = noResponseCounts.get(key);
            for (int i = 0; i < noResponses; i++)
                list.add(300d);
        }
        return list;
    }


    public Distances getResults(String key, boolean penalty) {
        return calculateResult(key, penalty);
    }
}
