package com.example.apivisualisation.data;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.apivisualisation.data.classes.AddressCollector;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class AdressEvaluator {
    HashMap<String, AddressCollector> addressCollectors = new HashMap<>();

    public void createNew(String key) {
        addressCollectors.put(key, new AddressCollector());
    }

    /**
     * feeds cityevaluation and calculates the adress results
     * sample: {"große Stadt", {obj}}
     * WARNING: key for service is here the to be tested service. The correct accepted is subkey
     * [testing-service, correct-accepted-service]
     */
    public void feed(String evalKey, CityEvaluation obj) {
        if (!addressCollectors.containsKey(evalKey))
            createNew(evalKey);
        var adressStatisticData = obj.getAddressStatisticData();

        for (var service : obj.getAddressStatisticData().getAddressResultForService().keySet()) {
            var result = adressStatisticData.getAddressResultForService().get(service);
            for (var correctAccepted : result.getAddressDistanceByCorrectAccepted().keySet()) {
                var collector = addressCollectors.get(evalKey);

                collector.add(service + "-" + correctAccepted, result, correctAccepted);
            }
        }

    }

    public AdressInformation getData(String collectionKey, String serviceResults, String correctAccepted, boolean distancePenalty) {
        return addressCollectors.get(collectionKey).getInformation(serviceResults + "-" + correctAccepted, distancePenalty);
    }
}

