package com.example.apivisualisation.data;

import lombok.Data;

@Data
public class Distances {
    public String key;
    public double average;
    public double median;
    public double percentile90;
    public double percentile95;
    public double percentile99;
}
