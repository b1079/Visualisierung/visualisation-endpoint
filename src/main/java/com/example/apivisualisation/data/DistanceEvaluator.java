package com.example.apivisualisation.data;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.data.distance.DistanceResult;
import com.example.apivisualisation.data.classes.DistanceCollector;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class DistanceEvaluator {
    HashMap<String, DistanceCollector> distanceEvaluations = new HashMap<>();

    /**
     * creates new distance evaluation based on key
     * key indicates which selector should be used for saving the results
     * example: ("ALL") => All cities from the dataset will be present
     * ("große Mittelstadt") => Categories from @see GroupingCategory
     */
    public void createNew(String key) {
        distanceEvaluations.put(key, new DistanceCollector());
    }

    /**
     * feeds the cityEvaluationStatisticData of the city which is interspected
     * no checks are performed whether the cityEvaluationStatisticData is connected to the key in any way
     */
    public void feedDistanceEvaluation(String evalKey, CityEvaluation cityEval) {
        if (!distanceEvaluations.containsKey(evalKey))
            createNew(evalKey);
        var cityEvaluationStatisticData = cityEval.getCityEvaluationStatisticData();
        List<DistanceResult> results = cityEvaluationStatisticData.getPointDistances();
        for (int index = 0; index < results.size(); index++) {
            var distanceResult = results.get(index);
            DistanceCollector distanceCollector = distanceEvaluations.get(evalKey);
            for (int i = 0; i < distanceResult.getOtherApis().size(); i++) {
                // other information like cityevaluation are not necessary for analysis only for debugging of very high request numbers
                distanceCollector.add(distanceResult.getCorrectAccepted() + distanceResult.getOtherApis().get(i),
                        distanceResult, cityEval, index, distanceResult.getOtherApis().get(i));
            }
        }
    }

    /**
     * returns the distance result of the key
     */
    public Distances getDistanceResult(String key, String serviceKey, boolean penalty) {
        if (!distanceEvaluations.containsKey(key))
            return null;
        return distanceEvaluations.get(key).getResults(serviceKey, penalty);
    }

    public DistanceCollector getDistanceCollector(String key) {
        return distanceEvaluations.get(key);
    }

}
