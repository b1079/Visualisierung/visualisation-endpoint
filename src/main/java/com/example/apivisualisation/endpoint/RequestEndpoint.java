package com.example.apivisualisation.endpoint;

import com.example.analyser.randomAnalyse.repos.CityEvaluationRepository;
import com.example.analyser.randomAnalyse.repos.CityTestingDataRepository;
import com.example.analyser.utility.SimilarityService;
import com.example.apivisualisation.data.InformationForCity;
import com.example.apivisualisation.service.CityEvaluationResultLoader;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import model.geometry.GeoJSONPoint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Getter
@Slf4j
public class RequestEndpoint {
    final CityTestingDataRepository cityTestingDataRepository;
    final CityEvaluationRepository cityEvaluationRepository;
    final SimilarityService similarityService;
    final CityEvaluationResultLoader cityEvaluationResultLoader;

    /**
     * returns information for the city
     * containing name and other data
     * including the tests which are performed
     */
    @GetMapping("/pointsForCity")
    public InformationForCity getLocationsForCity(String city) {
        var cityEvaluation = cityEvaluationRepository.findCityEvaluationByCity_Gemeindename(city);

        return new InformationForCity(city,
                GeoJSONPoint.fromLngLat(cityEvaluation.getCity().getLon(), cityEvaluation.getCity().getLat()),
                cityEvaluation.getCityPoints());
    }

    /**
     * searches for the plz in the database.
     *
     * @return sum of the samples taken for the plz
     */
    @GetMapping("/pointsPerPLZ")
    public int getPointsPerPlz(String plz) {
        if (plz == null || plz.equals("null"))
            return 0;


        if (cityEvaluationResultLoader.getAllCitiesByPlz().containsKey(plz)) {
            var foundCities = cityEvaluationResultLoader.getAllCitiesByPlz().get(Double.parseDouble(plz) + "");
            int sum = foundCities.stream().mapToInt((value -> value.getPointsPerCity())).sum();
            return sum;

        }
        return 0;
    }


    /**
     * @returns the services which are available
     */
    @GetMapping("/services")
    public List<String> getServices() {
        return List.of("Nominatim-API", "GoogleAPI", "Here-API");
    }

    /**
     * since every city has only one entry in the database, and some cities have more than one plz
     * only one is looked at. This function helps to find the used plz for the city.
     *
     * @return gets the used plz for the cityname
     */
    @GetMapping(value = "/savedPlzForCity")
    public String plzforCity(String city, String plz) {
        String cityCorrect = specicalCaseMapping(city);
        if (cityEvaluationResultLoader.getAllCitiesByName().containsKey(cityCorrect)) {
            if (cityEvaluationResultLoader.getAllCitiesByName()
                    .get(cityCorrect).size() > 1) {
                var res = cityEvaluationResultLoader.getAllCitiesByName().get(cityCorrect).stream().min(
                        Comparator.comparing(cityEvaluation -> scorePlzComparision(cityEvaluation.getCity().getPlz(), plz)));
                if (res.isEmpty()) {
                    log.error("empty res");
                    return "ERROR";
                }
                if (scorePlzComparision(res.get().getCity().getPlz(), plz) > 3) {
                    return res.get().getCity().getPlz();
                }
            }
            return cityEvaluationResultLoader.getAllCitiesByName()
                    .get(cityCorrect).get(0).getCity().getPlz();
        }

        var similarCities = cityEvaluationResultLoader.getAllCities().stream()
                .min(Comparator.comparingInt(o ->
                        (similarityService.distanceWithPattern(o.getCity().getGemeindename(), cityCorrect))
                                - scorePlzComparision(o.getCity().getPlz(), plz)));

        if (scorePlzComparision(similarCities.get().getCity().getPlz(), plz) < 3) {
            log.info("mapped {} to {} plz city: {} plz requested: {}",
                    cityCorrect, similarCities.get().getCity().getGemeindename(), similarCities.get().getCity().getPlz(),
                    plz);
        }

        return similarCities.get().getCity().getPlz();
    }

    private String specicalCaseMapping(String str) {
        if (str.equals("Oldenburg")) {
            return "Oldenburg (Oldenburg)";
        }
        if (str.equals("Senftenberg")) {
            return "Senftenberg/Zły Komorow";
        }
        if (str.equals("Gars am Inn"))
            return "Gars a.Inn";

        if (str.equals("Neuhausen/Spree"))
            return "Neuhausen/Spree / Kopańce/Sprjewja";

        if (str.equals("Briesen"))
            return "Briesen/Brjazyna";

        if (str.equals("Kolkwitz"))
            return "Kolkwitz/Gołkojce";
        if (str.equals("Neupetershain"))
            return "Neupetershain/Nowe Wiki";
        if (str.equals("Welzow"))
            return "Welzow/Wjelcej";
        if (str.equals("Felixsee"))
            return "Felixsee/Feliksowy Jazor";
        if (str.equals("Drachhausen"))
            return "Drachhausen/Hochoza";
        if (str.equals("Lübbenau/Spreewald"))
            return "Lübbenau/Spreewald / Lubnjow/Błota";
        if(str.equals("Vetschau/Spreewald"))
            return "Vetschau/Spreewald / Wětošow/Błota";
        if(str.equals("Bernau"))
            return "Bernau bei Berlin";
        if(str.equals("Ueckermünde"))
            return "Ueckermünde, Seebad";
        if(str.equals("Kühlungsborn"))
            return "Kühlungsborn, Ostseebad";
        if(str.equals("Korbach"))
            return "Korbach, Hansestadt";
        if(str.equals("Osterode"))
            return "Osterode am Harz";
        if(str.equals("Erkrath"))
            return "Erkrath, Fundort des Neanderthalers";
        if(str.equals("Dissen"))
            return "Dissen am Teutoburger Wald";
        if(str.equals("Kronberg"))
            return "Kronberg im Taunus";
        if(str.equals("Gelnhausen"))
            return "Gelnhausen, Barbarossast.";
        if(str.equals("Hochheim"))
            return "Hochheim am Main";
        if(str.equals("Hattersheim"))
            return "Hattersheim am Main";
        if(str.equals("Bad Soden"))
            return "Bad Soden am Taunus";
        if(str.equals("Liederbach"))
            return "Liederbach am Taunus";
        if(str.equals("Eichstetten"))
            return "Eichstetten am Kaiserstuhl";
        if(str.equals("Haag in Oberbayern"))
            return "Haag i.OB";

        return str;
    }

    private int scorePlzComparision(String plzExpected, String plz) {
        int score = 0;
        double diffrenz = Math.abs(Double.parseDouble(plzExpected) - Double.parseDouble(plz));
        if (diffrenz < 20)
            score += 3;
        plzExpected = plzDoubleToPlzString(plzExpected);
        plz = plzDoubleToPlzString(plz);
        if (plz.charAt(0) == plzExpected.charAt(0))
            score += 1;

        return score;
    }

    private String plzDoubleToPlzString(String plz) {
        if (plz.split("[.]", 2)[0].length() == 4) {
            plz = "0" + plz.split("[.]", 2)[1];
        }
        return plz;
    }

}
