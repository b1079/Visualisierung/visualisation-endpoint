package com.example.apivisualisation.endpoint;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.repos.CityEvaluationRepository;
import com.example.analyser.randomAnalyse.repos.CityTestingDataRepository;
import com.example.apivisualisation.data.AdressEvaluator;
import com.example.apivisualisation.data.AdressInformation;
import com.example.apivisualisation.service.CityEvaluationResultLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AdressEndpoint {
    CityTestingDataRepository cityTestingDataRepository;
    CityEvaluationRepository cityEvaluationRepository;
    CityEvaluationResultLoader resultLoader;
    AdressEvaluator adressEvaluator;

    // preloads and generates all distance results for specified keys to save calculation and response time
    AdressEndpoint(CityTestingDataRepository cityTestingDataRepository, CityEvaluationRepository cityEvaluationRepository,
                     CityEvaluationResultLoader resultLoader, AdressEvaluator adressEvaluator) {
        this.cityTestingDataRepository = cityTestingDataRepository;
        this.cityEvaluationRepository = cityEvaluationRepository;
        this.resultLoader = resultLoader;
        this.adressEvaluator = adressEvaluator;
        for (var cityEval : resultLoader.getAllCities()) {
            var categories = getCategories(cityEval);
            for (var key : categories)
                adressEvaluator.feed(key, cityEval);
        }
        System.out.println("Adress Endpoint loaded");


    }


    private List<String> getCategories(CityEvaluation cityEvaluation) {
        ArrayList<String> categories = new ArrayList<>(cityEvaluation.getCityEvaluationStatisticData().getCityCategories().values());
        categories.add(cityEvaluation.getCity().getPlz());
        categories.add("ALL");
        return categories;
    }

    @GetMapping("/addressResultsPerPlz")
    public AdressInformation getAdressResults(String key, String serviceResult, String correctAccepted, boolean penalty) {
        var res = adressEvaluator.getData(key,serviceResult, correctAccepted, penalty);
        return res;
    }
}
