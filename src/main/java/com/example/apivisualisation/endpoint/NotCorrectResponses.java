package com.example.apivisualisation.endpoint;

import com.example.analyser.randomAnalyse.data.cityevaluation.Category;
import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.repos.CityEvaluationRepository;
import com.example.analyser.randomAnalyse.repos.CityTestingDataRepository;
import com.example.apivisualisation.service.CityEvaluationResultLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
/**
 * rest controller which delivers the data for the
 */
public class NotCorrectResponses {
    @Autowired
    RequestEndpoint requestEndpoint;
    @Autowired
    CityTestingDataRepository cityTestingDataRepository;
    @Autowired
    CityEvaluationRepository cityEvaluationRepository;
    @Autowired
    CityEvaluationResultLoader cityEvaluationResultLoader;

    /**
     * gets noHouseNumber requests for plz the return value is the percentage of no house_number categories against all samples
     */
    @GetMapping("/noHouseNumber")
    double notCorrectResponsesPerPlz(String plz, String service) {
        if (plz == null || plz.equals("null"))
            return 0;
        List<String> noHouseNumber = List.of(Category.NO_HOUSE_NUMBER.getValue());
        return averageOfCategory(plz, service, noHouseNumber);
    }

    /**
     * gets noHouseNumber requests for plz the return value is the percentage of no house_number categories against all samples
     */
    @GetMapping("/noCity")
    double noCity(String plz, String service) {
        if (plz == null || plz.equals("null"))
            return 0;
        List<String> wrongCityValue = List.of(Category.WRONG_CITY.getValue());
        return averageOfCategory(plz, service, wrongCityValue);
    }

    private double averageOfCategory(String plz, String service, List<String> noHouseNumber) {
        var plzCorrected = Double.parseDouble(plz) + "";

        if (cityEvaluationResultLoader.getAllCitiesByPlz().containsKey(plzCorrected)) {
            double points = cityEvaluationResultLoader.getAllCitiesByPlz().get(plzCorrected)
                    .stream().mapToDouble((value -> getErrorCategories(value.getCityEvaluationStatisticData()
                            .getCategoryPerService().get(service), noHouseNumber))).sum();
            double totalPoints = cityEvaluationResultLoader.getAllCitiesByPlz().get(plzCorrected)
                    .stream().mapToDouble((CityEvaluation::getPointsPerCity)).sum() / 3;
            return points / totalPoints;

        }
        return 1.0;
    }

    @GetMapping("/noStreetByState")
    HashMap<String, Double> averagesForStates(String service) {
        HashMap<String, Double> map = new HashMap<>();
        var states = cityEvaluationResultLoader.getCitiesByState();
        for (var key : states.keySet()) {
            map.put(key, calculateAverage(service, states.get(key)));
        }
        return map;
    }

    /**
     * returns the percentage of noStreet results by city size
     *
     */
    @GetMapping("/noStreetByCitySize")
    Map<String, Double> noStreetsByCitySize(String service, String categoryType) {
        Map<String, Integer> itemCount = new HashMap<>();
        Map<String, Integer> occurrenceCount = new HashMap<>();
        var cities = cityEvaluationResultLoader.getAllCities();
        for (var eval : cities) {
            var category = eval.getCityEvaluationStatisticData().getCityCategories().get(categoryType);
            if (!itemCount.containsKey(category)) {
                itemCount.put(category, 0);
                occurrenceCount.put(category, 0);
            }
            itemCount.put(category, itemCount.get(category) + eval.getPointsPerCity() / 3);
            occurrenceCount.put(category,
                    occurrenceCount.get(category) + getErrorCategories(eval.getCityEvaluationStatisticData()
                            .getCategoryPerService().get(service), List.of(Category.NO_HOUSE_NUMBER.toString())));
        }
        Map<String,Double> result = new HashMap<>();
        for(var key : itemCount.keySet())
            result.put(key, (double) occurrenceCount.get(key) / itemCount.get(key));
        return result;
    }

    private double calculateAverage(String service, List<CityEvaluation> evaluations) {
        int points = 0;
        int errors = 0;

        for (var eval : evaluations) {
            points += eval.getPointsPerCity() / 3;
            errors += getErrorCategories(eval.getCityEvaluationStatisticData().getCategoryPerService().get(service),
                    List.of(Category.NO_HOUSE_NUMBER.toString()));
        }
        if (points == 0)
            return 0d;
        return (double) errors / points;
    }

    private int getErrorCategories(Map<String, Integer> map, List<String> categoriesToCount) {
        int count = 0;
        for (var key : map.keySet()) {
            if (categoriesToCount.contains(key)) {
                count += map.get(key);
            }
        }
        return count;
    }


}
