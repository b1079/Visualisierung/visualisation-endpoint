package com.example.apivisualisation.endpoint;

import com.example.analyser.randomAnalyse.data.cityevaluation.CityEvaluation;
import com.example.analyser.randomAnalyse.repos.CityEvaluationRepository;
import com.example.analyser.randomAnalyse.repos.CityTestingDataRepository;
import com.example.apivisualisation.data.DistanceEvaluator;
import com.example.apivisualisation.data.Distances;
import com.example.apivisualisation.service.CityEvaluationResultLoader;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DistanceEndpoint {
    CityTestingDataRepository cityTestingDataRepository;
    CityEvaluationRepository cityEvaluationRepository;
    CityEvaluationResultLoader resultLoader;
    DistanceEvaluator distanceEvaluator;

    // preloads and generates all distance results for specified keys to save calculation and response time
    DistanceEndpoint(CityTestingDataRepository cityTestingDataRepository, CityEvaluationRepository cityEvaluationRepository,
                     CityEvaluationResultLoader resultLoader, DistanceEvaluator distanceEvaluator) {
        this.cityTestingDataRepository = cityTestingDataRepository;
        this.cityEvaluationRepository = cityEvaluationRepository;
        this.resultLoader = resultLoader;
        this.distanceEvaluator = distanceEvaluator;
        for (var cityEval : resultLoader.getAllCities()) {
            var categories = getCategories(cityEval);
            for (var key : categories)
                distanceEvaluator.feedDistanceEvaluation(key, cityEval);
            distanceEvaluator.feedDistanceEvaluation("ALL", cityEval);
        }
    }


    private List<String> getCategories(CityEvaluation cityEvaluation) {
        ArrayList<String> categories = new ArrayList<>(cityEvaluation.getCityEvaluationStatisticData().getCityCategories().values());
        categories.add(cityEvaluation.getCity().getPlz());
        categories.add("ALL");
        return categories;
    }

    @GetMapping("/distancePerPlz")
    public Distances getDistanceForPlz(String plz, String correct, String service) {
        var res = distanceEvaluator.getDistanceResult(Double.parseDouble(plz) + "", correct + service, true);
        return res;
    }

}
